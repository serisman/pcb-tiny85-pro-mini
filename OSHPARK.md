# Tiny85 Pro Mini v1.0 #

$0.70 each ($2.10 for 3)

[https://bitbucket.org/serisman/pcb-tiny85-pro-mini](https://bitbucket.org/serisman/pcb-tiny85-pro-mini)

ARDUINO COMPATIBLE - TO DO... more information about Arduino IDE configuration

This uses the same serial port pinout as the Arduino Pro Mini.  The software serial port is accomplished with a half-duplex RxTx pin on PB5 along with accompanying hardware to emulate separate RX/TX pins.  To use this, you will need to modify the fuses to turn off Reset AFTER burning the TSB bootloader.  This arrangement keeps all of the normal pins PB0-PB4 available for project use.

This board also uses an alternate method of performing reset (auto-reset supported) by using a mosfet to disconnect GND when the Reset line is low.

### Suggested Fuses: ###

* for 5V 16MHz operation: Low=0xF1, High=0xDC (0x5C after burning bootloader), Ext=0xFE
* for 3.3V 8MHz operation: Low=0xE2, High=0xDD (0x5D after burning bootloader), Ext=0xFE

### Suggested BOM: ###

* U1: SOIC-8 (wide!) - ATtiny85
* U2: SOT-23 - XC6206 (662k) 3.3v LDO regulator (6V max input), or 78L05 (L05) 5v regulator
* Q1: SOT-23 - Si2302DS N-Channel low RDS_ON mosfet
* Q2: SOT-23 - 2N7002 N-Channel mosfet
* D1: SOD-323 - 1N4148WS diode
* C1,C2: 0805 - 100nF capacitors
* C4,C5: 0805 - 1uF capacitors
* C3,C6: 0805 - 10uF capacitors
* R1: 0805 - 10k resistor
* R2: 0805 - 4.7k resistor
* SW1: - Momentary push button switch for Reset

![Schematic.png](https://bytebucket.org/serisman/pcb-tiny85-pro-mini/raw/master/output/Schematic.png)

![3D-Front.png](https://bytebucket.org/serisman/pcb-tiny85-pro-mini/raw/master/output/3D-Front.png)
![3D-Back.png](https://bytebucket.org/serisman/pcb-tiny85-pro-mini/raw/master/output/3D-Back.png)
